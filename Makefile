CC=gcc -Wall -g

CFLAGS=$(shell pkg-config --cflags glib-2.0) $(shell pkg-config --cflags gnome-vfs-2.0) $(shell pkg-config --cflags gtk+-2.0 sqlite) -g   
LDFLAGS = $(shell pkg-config --libs gobject-2.0 gnome-vfs-2.0 gtk+-2.0 sqlite3)

DEFS=$(shell pkg-config --variable=defsdir pygtk-2.0)
PY_CFLAGS = $(shell pkg-config --cflags gtk+-2.0 pygtk-2.0) -I/usr/include/python2.5/ 
PY_LDFLAGS = $(LDFLAGS) $(shell pkg-config --libs pygtk-2.0)

OBJ_FILES = ephy-history.o \
			ephy-history-node.o \
			ephy-history-page-node.o \
			ephy-history-visit-node.o \
			ephy-history-marshal.o

.SUFFIXES: .c .o

.c.o:
	$(CC) -c $(CFLAGS) $(PY_CFLAGS) $<

test: $(OBJ_FILES) test.o
	$(CC) $(OBJ_FILES) test.o $(CFLAGS) $(LDFLAGS) -o test 

completion-test: $(OBJ_FILES) ephy-completion-model.o completion-test.o 
	$(CC) $(OBJ_FILES) ephy-completion-model.o completion-test.o $(CFLAGS) $(LDFLAGS) -o completion-test

ephyhistory.c: ephyhistory.defs ephyhistory.override            
	pygtk-codegen-2.0 --prefix ephyhistory \
	--register $(DEFS)/gdk-types.defs \
	--register $(DEFS)/gtk-types.defs \
	--override ephyhistory.override \
	ephyhistory.defs > $@         

ephyhistory.so: $(OBJ_FILES) ephyhistory.o ephyhistorymodule.o
	$(CC) $(PY_LDFLAGS) -shared $^ -o $@ 

clean: 
	rm -rf *.o *.so test completion-test *~
