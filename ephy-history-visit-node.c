#include "ephy-history-visit-node.h"

G_DEFINE_TYPE(EphyHistoryVisitNode, ephy_history_visit_node, EPHY_TYPE_HISTORY_NODE)
#define EPHY_HISTORY_VISIT_NODE_GET_PRIVATE(object)   (G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_HISTORY_VISIT_NODE, EphyHistoryVisitNodePrivate))

enum
{
    EPHY_HISTORY_VISIT_NODE_PROP_DATE = 1,
    EPHY_HISTORY_VISIT_NODE_PROP_TYPE,
    EPHY_HISTORY_VISIT_NODE_PROP_DURATION,
    EPHY_HISTORY_VISIT_NODE_PROP_REF_ID,
};

static void ephy_history_visit_node_finalize (GObject *object);

struct _EphyHistoryVisitNodePrivate
{
    guint64 date;
    guint8 type;
    guint32 duration;
    guint64 ref_id;
};

static void
ephy_history_visit_node_set_property (GObject *object,
               guint prop_id,
               const GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryVisitNode *self = EPHY_HISTORY_VISIT_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_VISIT_NODE_PROP_DATE:
            self->priv->date = g_value_get_uint64 (value);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_TYPE:
            self->priv->type = g_value_get_uchar (value);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_DURATION:
            self->priv->duration = g_value_get_uint (value);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_REF_ID:
            self->priv->ref_id = g_value_get_uint64 (value);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_visit_node_get_property (GObject *object,
               guint prop_id,
               GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryVisitNode *self = EPHY_HISTORY_VISIT_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_VISIT_NODE_PROP_DATE:
            g_value_set_uint64 (value, self->priv->date);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_TYPE:
            g_value_set_uchar (value, self->priv->type);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_DURATION:
            g_value_set_uint (value, self->priv->duration);
            break;
        case EPHY_HISTORY_VISIT_NODE_PROP_REF_ID:
            g_value_set_uint64 (value, self->priv->ref_id);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_visit_node_class_init (EphyHistoryVisitNodeClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = ephy_history_visit_node_finalize;

    object_class->get_property = ephy_history_visit_node_get_property;
    object_class->set_property = ephy_history_visit_node_set_property;

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_VISIT_NODE_PROP_DATE,
                     g_param_spec_uint64 ("date",
                                "date",
                                "Time of visit",
                                0  /* minimum value */,
                                G_MAXUINT64 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_VISIT_NODE_PROP_TYPE,
                     g_param_spec_uchar ("type",
                                "type",
                                "Type of visit",
                                0  /* minimum value */,
                                G_MAXUINT8 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_VISIT_NODE_PROP_DURATION,
                     g_param_spec_uint ("duration",
                                "duration",
                                "Duration of Visit in Seconds",
                                0  /* minimum value */,
                                G_MAXUINT32 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_VISIT_NODE_PROP_REF_ID,
                     g_param_spec_uint64 ("ref_id",
                                "ref_id",
                                "ID of Referring visit",
                                0  /* minimum value */,
                                G_MAXUINT64 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_type_class_add_private (object_class, sizeof (EphyHistoryVisitNodePrivate));
}

static void
ephy_history_visit_node_init (EphyHistoryVisitNode *node)
{
    node->priv = EPHY_HISTORY_VISIT_NODE_GET_PRIVATE (node);
}

static void
ephy_history_visit_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (ephy_history_visit_node_parent_class)->finalize (object);
}


