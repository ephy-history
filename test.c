#include <stdio.h>
#include "ephy-history.h"

int main() {
	EphyHistory *eh;
	EphyHistoryPageNode *node;
    GPtrArray *hosts;
    GPtrArray *pages;
    GArray *page_ids;
	int retval;
    int i;
    gint64 id, last_visit;
    guint visit_count;
    char *url, *title, *favicon_url;

	g_type_init ();
    gnome_vfs_init();

	eh = ephy_history_new();
	printf("History Enabled: %d\n", ephy_history_is_enabled(eh));

	retval = ephy_history_add_page(eh, "http://www.linux.org/index.html", FALSE, TRUE,
				       NULL);
	printf("Add Page: %d\n", retval);

	retval = ephy_history_add_page(eh, "http://www.gnome.org/index.htm", FALSE, TRUE,
				       NULL);
	printf("Add Page: %d\n", retval);

	retval = ephy_history_is_page_visited(eh, "http://www.gnome.org/index.htm");
	printf("Is Visited: %d\n", retval);

	retval = ephy_history_set_page_title(eh, "http://www.gnome.org", "GNOME");
	printf("Set Title: %d\n", retval);

	visit_count = ephy_history_get_page_visit_count(eh, "http://www.gnome.org/index.htm");
	printf("Visit Count = %u: \n", visit_count);

    retval = ephy_history_set_icon(eh, "http://www.gnome.org", 
                                   "http://www.gnome.org/favicon.ico");
	printf("set_icon: %d\n", retval);
	printf("get_icon: %s\n", ephy_history_get_icon(eh, "http://www.gnome.org"));

    hosts = ephy_history_get_hosts (eh);
    g_print("ephy_history_get_hosts:\n");
    for(i = 0; i < hosts->len; i++) {
        node = g_ptr_array_index (hosts, i);
        g_object_get (G_OBJECT (node), "id", &id, "url", &url, "title", &title,
                      "favicon-url", &favicon_url, "visit-count", &visit_count, 
                      "last-visit", &last_visit, NULL);
        g_print("id: %llu url: %s title: %s favicon_url: %s visit_count: %u "
                "last_visit: %llu\n", id, url, title, favicon_url, visit_count, 
                last_visit);
        g_object_unref (G_OBJECT (node));
    }
    g_ptr_array_free(hosts, TRUE);

    pages = ephy_history_get_pages (eh);
    g_print("ephy_history_get_pages:\n");
    for(i = 0; i < pages->len; i++) {
        node = g_ptr_array_index (pages, i);
        g_object_get (G_OBJECT (node), "id", &id, "url", &url, "title", &title,
                      "favicon-url", &favicon_url, "visit-count", &visit_count, 
                      "last-visit", &last_visit, NULL);
        g_print("id: %llu url: %s title: %s favicon_url: %s visit_count: %u "
                "last_visit: %llu\n", id, url, title, favicon_url, visit_count, 
                last_visit);
        g_object_unref (G_OBJECT (node));
    }
    g_ptr_array_free(pages, TRUE);

    page_ids = ephy_history_get_page_ids (eh);
    g_print("ephy_history_get_page_ids:\n");
    for(i = 0; i < page_ids->len; i++) {
        guint64 id64 = g_array_index (page_ids, guint64, i);
        printf ("%llu\n", id64);

        node = ephy_history_get_page_by_id (eh, id64);
        g_print("ephy_history_get_page_by_id:\n");
        g_object_get (G_OBJECT (node), "id", &id, "url", &url, "title", &title,
                      "favicon-url", &favicon_url, "visit-count", &visit_count, 
                      "last-visit", &last_visit, NULL);

        g_print("id: %llu url: %s title: %s favicon_url: %s visit_count: %u "
                "last_visit: %llu\n", id, url, title, favicon_url, visit_count, 
                last_visit);
        g_object_unref (G_OBJECT (node));
    }
    g_array_free(page_ids, TRUE);

    g_object_unref (G_OBJECT (eh));

    gnome_vfs_shutdown();
	return 0;
}
