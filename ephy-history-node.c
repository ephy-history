#include "ephy-history-node.h"

G_DEFINE_TYPE(EphyHistoryNode, ephy_history_node, G_TYPE_OBJECT)
#define EPHY_HISTORY_NODE_GET_PRIVATE(object)   (G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_HISTORY_NODE, EphyHistoryNodePrivate))

enum
{
    EPHY_HISTORY_NODE_PROP_ID = 1,
    EPHY_HISTORY_NODE_PROP_URL,
    EPHY_HISTORY_NODE_PROP_TITLE,
    EPHY_HISTORY_NODE_PROP_FAVICON_URL,
};

static void ephy_history_node_finalize (GObject *object);

struct _EphyHistoryNodePrivate
{
    guint64 id;
    gchar *url;
    gchar *title;
    gchar *favicon_url;
};

static void
ephy_history_node_set_property (GObject *object,
               guint prop_id,
               const GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryNode *self = EPHY_HISTORY_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_NODE_PROP_ID:
            self->priv->id = g_value_get_uint64 (value);
            break;
        case EPHY_HISTORY_NODE_PROP_URL:
            g_free (self->priv->url);
            self->priv->url = g_value_dup_string (value);
            break;
        case EPHY_HISTORY_NODE_PROP_TITLE:
            g_free (self->priv->title);
            self->priv->title = g_value_dup_string (value);
            break;
        case EPHY_HISTORY_NODE_PROP_FAVICON_URL:
            g_free (self->priv->favicon_url);
            self->priv->favicon_url = g_value_dup_string (value);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_node_get_property (GObject *object,
               guint prop_id,
               GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryNode *self = EPHY_HISTORY_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_NODE_PROP_ID:
            g_value_set_uint64 (value, self->priv->id);
            break;
        case EPHY_HISTORY_NODE_PROP_URL:
            g_value_set_string (value, self->priv->url);
            break;
        case EPHY_HISTORY_NODE_PROP_TITLE:
            g_value_set_string (value, self->priv->title);
            break;
        case EPHY_HISTORY_NODE_PROP_FAVICON_URL:
            g_value_set_string (value, self->priv->favicon_url);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_node_class_init (EphyHistoryNodeClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = ephy_history_node_finalize;

    object_class->get_property = ephy_history_node_get_property;
    object_class->set_property = ephy_history_node_set_property;

    g_type_class_add_private (object_class, sizeof (EphyHistoryNodePrivate));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_NODE_PROP_ID,
                     g_param_spec_uint64 ("id",
                                "id",
                                "item id",
                                0  /* minimum value */,
                                G_MAXUINT64 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_NODE_PROP_URL,
                     g_param_spec_string ("url",
                                "url",
                                "Item URL",
                                NULL  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_NODE_PROP_TITLE,
                     g_param_spec_string ("title",
                                "title",
                                "Item Title",
                                NULL /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_NODE_PROP_FAVICON_URL,
                     g_param_spec_string ("favicon-url",
                                "favicon-url",
                                "Favicon URL",
                                NULL  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
 
}

static void
ephy_history_node_init (EphyHistoryNode *node)
{
    node->priv = EPHY_HISTORY_NODE_GET_PRIVATE (node);
}

static void
ephy_history_node_finalize (GObject *object)
{
    EphyHistoryNode *self = EPHY_HISTORY_NODE (object);

    g_free (self->priv->url);
    g_free (self->priv->title);
    g_free (self->priv->favicon_url);
/*
    g_free (self->priv);
*/
    G_OBJECT_CLASS (ephy_history_node_parent_class)->finalize (object);
}


