#include "ephy-history-page-node.h"

G_DEFINE_TYPE(EphyHistoryPageNode, ephy_history_page_node, EPHY_TYPE_HISTORY_NODE)
#define EPHY_HISTORY_PAGE_NODE_GET_PRIVATE(object)   (G_TYPE_INSTANCE_GET_PRIVATE ((object), EPHY_TYPE_HISTORY_PAGE_NODE, EphyHistoryPageNodePrivate))

enum
{
    EPHY_HISTORY_PAGE_NODE_PROP_VISIT_COUNT = 1,
    EPHY_HISTORY_PAGE_NODE_PROP_LAST_VISIT,
};

static void ephy_history_page_node_finalize (GObject *object);

struct _EphyHistoryPageNodePrivate
{
    guint32 visit_count;
    guint64 last_visit;
};

static void
ephy_history_page_node_set_property (GObject *object,
               guint prop_id,
               const GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryPageNode *self = EPHY_HISTORY_PAGE_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_PAGE_NODE_PROP_VISIT_COUNT:
            self->priv->visit_count = g_value_get_uint (value);
            break;
        case EPHY_HISTORY_PAGE_NODE_PROP_LAST_VISIT:
            self->priv->last_visit = g_value_get_uint64 (value);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_page_node_get_property (GObject *object,
               guint prop_id,
               GValue *value,
               GParamSpec *pspec)
{
    EphyHistoryPageNode *self = EPHY_HISTORY_PAGE_NODE (object);

    switch (prop_id)
    {
        case EPHY_HISTORY_PAGE_NODE_PROP_VISIT_COUNT:
            g_value_set_uint (value, self->priv->visit_count);
            break;
        case EPHY_HISTORY_PAGE_NODE_PROP_LAST_VISIT:
            g_value_set_uint64 (value, self->priv->last_visit);
            break;
        default:
            /* We don't have any other property... */
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
ephy_history_page_node_class_init (EphyHistoryPageNodeClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = ephy_history_page_node_finalize;

    object_class->get_property = ephy_history_page_node_get_property;
    object_class->set_property = ephy_history_page_node_set_property;

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_PAGE_NODE_PROP_VISIT_COUNT,
                     g_param_spec_uint ("visit-count",
                                "visit_count",
                                "Page Visit count",
                                0  /* minimum value */,
                                G_MAXUINT32 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_object_class_install_property (object_class,
                     EPHY_HISTORY_PAGE_NODE_PROP_LAST_VISIT,
                     g_param_spec_uint64 ("last-visit",
                                "last_visit",
                                "Time of last visit",
                                0  /* minimum value */,
                                G_MAXUINT64 /* maximum value */,
                                0  /* default value */,
                                G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

    g_type_class_add_private (object_class, sizeof (EphyHistoryPageNodePrivate));
}

static void
ephy_history_page_node_init (EphyHistoryPageNode *node)
{
    node->priv = EPHY_HISTORY_PAGE_NODE_GET_PRIVATE (node);
}

static void
ephy_history_page_node_finalize (GObject *object)
{
    G_OBJECT_CLASS (ephy_history_page_node_parent_class)->finalize (object);
}

EphyHistoryPageNode *
ephy_history_page_node_new (guint64 id, 
                            const char *uri,
                            const char *title,
                            const char *favicon_uri, 
                            guint visit_count,
                            guint64 last_visit)
{
    EphyHistoryPageNode *node;

    node = EPHY_HISTORY_PAGE_NODE (g_object_new (EPHY_TYPE_HISTORY_PAGE_NODE,
                                                 "id", id, 
                                                 "url", uri,
                                                 "title", title,
                                                 "favicon-url", favicon_uri,
                                                 "visit-count", visit_count,
                                                 "last-visit", last_visit,
                                                 NULL));
    return node;
}

void
ephy_history_page_node_free (EphyHistoryPageNode *node)
{

}

