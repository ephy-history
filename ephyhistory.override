%%
headers
#include <Python.h>               
#include "pygobject.h"
#include "ephy-history.h"
#include "ephy-history-node.h"
#include "ephy-history-page-node.h"
#include "ephy-history-visit-node.h"

static PyObject *
_helper_wrap_gobject_gptrarray (GPtrArray *list, gboolean dealloc)
{
    PyObject *py_list;
    int i;

    if ((py_list = PyList_New(0)) == NULL) {
        return NULL;
    }
    for( i = 0; i < list->len; i++ ) {
        PyObject *obj = pygobject_new (G_OBJECT (g_ptr_array_index(list, i)));
        PyList_Append(py_list, obj);
        Py_DECREF(obj);
    }
    if (dealloc) g_ptr_array_free (list, TRUE);
    return py_list;
}

static PyObject *
_helper_wrap_guint64_garray (GArray *list, gboolean dealloc)
{
    PyObject *py_list;
    int i;

    if ((py_list = PyList_New(0)) == NULL) {
        return NULL;
    }
    for( i = 0; i < list->len; i++ ) {
        PyList_Append (py_list, PyLong_FromUnsignedLongLong (g_array_index (list, guint64, i))); 
    }
    if (dealloc) g_array_free (list, TRUE);
    return py_list;
}
%%
modulename ephyhistory                     
%%
import gobject.GObject as PyGObject_Type  
%%
ignore-glob
  *_get_type                            
%%
override ephy_history_get_hosts noargs
static PyObject *
_wrap_ephy_history_get_hosts (PyGObject *self)
{
    GPtrArray *list = ephy_history_get_hosts ((EphyHistory *) (self->obj));

    return _helper_wrap_gobject_gptrarray (list, FALSE);
}
%%
override ephy_history_get_pages noargs
static PyObject *
_wrap_ephy_history_get_pages (PyGObject *self)
{
    GPtrArray *list = ephy_history_get_pages ((EphyHistory *) (self->obj));

    return _helper_wrap_gobject_gptrarray (list, FALSE);
}
%%
override ephy_history_get_page_ids noargs
static PyObject *
_wrap_ephy_history_get_page_ids (PyGObject *self)
{
    GArray *list = ephy_history_get_page_ids ((EphyHistory *) (self->obj));

    return _helper_wrap_guint64_garray (list, FALSE);
}
