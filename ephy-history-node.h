#ifndef EPHY_HISTORY_NODE_H
#define EPHY_HISTORY_NODE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define EPHY_TYPE_HISTORY_NODE          (ephy_history_node_get_type ())

#define EPHY_HISTORY_NODE(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY_NODE, EphyHistoryNode))

#define EPHY_HISTORY_NODE_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY_NODE, EphyHistoryNodeClass))

#define EPHY_IS_HISTORY_NODE(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY_NODE))

#define EPHY_IS_HISTORY_NODE_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY_NODE))

#define EPHY_HISTORY_NODE_GET_CLASS(o)  (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY_NODE, EphyHistoryNodeClass))

typedef struct _EphyHistoryNodeClass    EphyHistoryNodeClass;
typedef struct _EphyHistoryNode         EphyHistoryNode;
typedef struct _EphyHistoryNodePrivate  EphyHistoryNodePrivate;

struct _EphyHistoryNode
{
    GObject parent;

    /*< private >*/
    EphyHistoryNodePrivate *priv;
};

struct _EphyHistoryNodeClass
{
    GObjectClass parent_class;
};

GType ephy_history_node_get_type (void);

G_END_DECLS

#endif /* EPHY_HISTORY_NODE_H */
