#ifndef EPHY_HISTORY_PAGE_NODE_H
#define EPHY_HISTORY_PAGE_NODE_H

#include <glib-object.h>
#include "ephy-history-node.h"

G_BEGIN_DECLS

#define EPHY_TYPE_HISTORY_PAGE_NODE          (ephy_history_page_node_get_type ())

#define EPHY_HISTORY_PAGE_NODE(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY_PAGE_NODE, EphyHistoryPageNode))

#define EPHY_HISTORY_PAGE_NODE_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY_PAGE_NODE, EphyHistoryPageNodeClass))

#define EPHY_IS_HISTORY_PAGE_NODE(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY_PAGE_NODE))

#define EPHY_IS_HISTORY_PAGE_NODE_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY_PAGE_NODE))

#define EPHY_HISTORY_PAGE_NODE_GET_CLASS(o)  (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY_PAGE_NODE, EphyHistoryPageNodeClass))

typedef struct _EphyHistoryPageNodeClass    EphyHistoryPageNodeClass;
typedef struct _EphyHistoryPageNode         EphyHistoryPageNode;
typedef struct _EphyHistoryPageNodePrivate  EphyHistoryPageNodePrivate;

struct _EphyHistoryPageNode
{
    EphyHistoryNode parent;

    /*< private >*/
    EphyHistoryPageNodePrivate *priv;
};

struct _EphyHistoryPageNodeClass
{
    EphyHistoryNodeClass parent_class;
};


GType                   ephy_history_page_node_get_type (void);

EphyHistoryPageNode     *ephy_history_page_node_new (guint64 id, 
                                                     const char *uri,
                                                     const char *title,
                                                     const char *favicon_uri, 
                                                     guint visit_count,
                                                     guint64 last_visit); 

void                    ephy_history_page_node_free (EphyHistoryPageNode *node);

G_END_DECLS

#endif /* EPHY_HISTORY_PAGE_NODE_H */
