#!/usr/bin/env python

from xml.sax import make_parser
from xml.sax.handler import ContentHandler 
from ephyhistory import History, HistoryPageNode
import sys
import os.path

class HistoryHandler(ContentHandler):
    def __init__ (self):
        self.propId = 0
        self.props = {}
        self.inProp = False
        self.history = History()

    def startElement(self, name, attrs): 
        # page item
        if name == 'node':
            self.props = {}

        # page properties
        elif name == 'property':
            self.propId = int(attrs.get('id'))
            self.inProp = True

    def endElement(self, name): 
        if name == 'node':
            # print self.props
            # insert into DB 
            self.createVisit(self.props)
        elif name == 'property':
            self.inProp = False
             
    def characters(self, ch): 
        if self.inProp:
            self.props[self.propId] = self.props.get(self.propId, '') + ch 

    def createVisit(self, props):
        # refer to enum in ephy-history.h for propId -> property mapping 

        # XXX: add a function to the API for specifying 
        # visit count and visit date when adding a page 
 
        url = props.get(3, None)
        if url:
            self.history.add_page(url, redirect=False, toplevel=False, ref_url='')
        title = props.get(2, None)
        if title:
            self.history.set_page_title(url, title.encode('utf-8'))
        favicon = props.get(9, None)
        if favicon:
            self.history.set_icon(url, favicon)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        xmlfile = os.path.expanduser('~/.gnome2/epiphany/ephy-history.xml')
    else:
        xmlfile = sys.argv[1]
    parser = make_parser()
    hh = HistoryHandler()
    parser.setContentHandler(hh)
    parser.parse(xmlfile)

   

