#include <gtk/gtk.h>
#include "ephy-completion-model.h"

#define ACTIVITY_PERIOD (2 * 100) /* every 2 seconds */

GRand *rand;
GPtrArray *pages_added = NULL;
GHashTable *pages_added_hash = NULL;
GPtrArray *pages = NULL;

gboolean history_activity_func (gpointer ptr) 
{
	int rc;
	EphyHistory *eh = (EphyHistory *) ptr;
	char *url;
	int i;

	g_print("+ %d / %d\n", pages_added->len, pages->len);
	switch (g_rand_int_range (rand, 0, 5))
	{
		/* add a page */
		case 0:
		case 1:
			if (pages->len <= 0) 
				return TRUE;
	
			i = g_rand_int_range (rand, 0, pages->len);
			url = g_ptr_array_remove_index_fast (pages, i); 
			g_print("Add Page %s\n", url);
			rc = ephy_history_add_page(eh, url, FALSE, TRUE, NULL);
			g_ptr_array_add (pages_added, url);
			break;
		/* add a visit */
		case 2:
		case 3:
			if (pages_added->len <= 0) 
				return TRUE;

			i = g_rand_int_range (rand, 0, pages_added->len);
			url = g_ptr_array_index (pages_added, i); 
			g_print("Add Visit %s\n", url);
			rc = ephy_history_add_page(eh, url, FALSE, TRUE, NULL);
			break;
	
		/* remove a page */
		case 4:
			if (pages_added->len <= 0) 
				return TRUE;
	
			i = g_rand_int_range (rand, 0, pages_added->len);
			url = g_ptr_array_remove_index_fast (pages_added, i); 
			g_print("Remove %s\n", url);
			rc = ephy_history_remove_page(eh, url);
			g_ptr_array_add (pages, url);
			break;
	}
	return TRUE;
}


void read_history (char *filename) 
{
	FILE *fp = NULL;
	char tmp[1000];
	char *url;

	fp = fopen(filename, "r");
	if (fp == NULL)
		return;
	
	while (!feof (fp)) 
	{
		fscanf(fp, "%s\n", tmp);
		url = g_strdup (tmp);	
		g_ptr_array_add (pages, url);  
	}
	fclose (fp);
}

int
main (int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *scrwin;
	GtkWidget *treeview;
	EphyCompletionModel *cModel;
	GtkTreeModel *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	EphyHistory *eh;
	int rc;

	gtk_init (&argc, &argv);


	/* model */
	eh = ephy_history_new ();
	cModel = ephy_completion_model_new (eh);
	model = GTK_TREE_MODEL (cModel);

	/* load data for the model */
	rand = g_rand_new ();
	pages_added = g_ptr_array_new ();
	pages_added_hash = g_hash_table_new (g_int_hash, g_int_equal);
	pages = g_ptr_array_new ();
	read_history (argv[1]);
	g_timeout_add (ACTIVITY_PERIOD, history_activity_func, eh);
	
	/* view */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);
	g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit),
			  NULL);

	scrwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrwin),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (window), scrwin);

	treeview = gtk_tree_view_new_with_model (model);
	g_object_unref (model);
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
						     -1, "URL",
						     renderer,
						     "text",
						     EPHY_COMPLETION_ACTION_COL,
						     NULL);
	column = gtk_tree_view_get_column(GTK_TREE_VIEW (treeview), 0);
	gtk_tree_view_column_set_expand(column, TRUE);
	gtk_tree_view_column_set_resizable(column, TRUE);
	
	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
						     -1, "Title",
						     renderer,
						     "text",
						     EPHY_COMPLETION_EXTRA_COL,
					     	     NULL);
	column = gtk_tree_view_get_column(GTK_TREE_VIEW (treeview), 1);
	gtk_tree_view_column_set_expand(column, TRUE);
	gtk_tree_view_column_set_resizable(column, TRUE);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (treeview),
						     -1, "Relevance",
						     renderer,
						     "text",
						     EPHY_COMPLETION_RELEVANCE_COL,
						     NULL);
	gtk_container_add (GTK_CONTAINER (scrwin), treeview);

	gtk_widget_show_all (window);

	gtk_main ();

	return 0;
}
