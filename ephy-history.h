#ifndef EPHY_HISTORY_H
#define EPHY_HISTORY_H

#include <glib-object.h>
#include "ephy-history-page-node.h"

G_BEGIN_DECLS
#define EPHY_TYPE_HISTORY           (ephy_history_get_type ())
#define EPHY_HISTORY(o)             (G_TYPE_CHECK_INSTANCE_CAST ((o), EPHY_TYPE_HISTORY, EphyHistory))
#define EPHY_HISTORY_CLASS(k)       (G_TYPE_CHECK_CLASS_CAST((k), EPHY_TYPE_HISTORY, EphyHistoryClass))
#define EPHY_IS_HISTORY(o)          (G_TYPE_CHECK_INSTANCE_TYPE ((o), EPHY_TYPE_HISTORY))
#define EPHY_IS_HISTORY_CLASS(k)    (G_TYPE_CHECK_CLASS_TYPE ((k), EPHY_TYPE_HISTORY))
#define EPHY_HISTORY_GET_CLASS(o)   (G_TYPE_INSTANCE_GET_CLASS ((o), EPHY_TYPE_HISTORY, EphyHistoryClass))
#define EPHY_DB_ERROR_QUARK   (ephy_db_error_quark)
typedef struct _EphyHistoryClass EphyHistoryClass;
typedef struct _EphyHistory EphyHistory;
typedef struct _EphyHistoryPrivate EphyHistoryPrivate;

struct _EphyHistory
{
	GObject parent;

	/*< private > */
	EphyHistoryPrivate *priv;
};

struct _EphyHistoryClass
{
	GObjectClass parent_class;

	/* Signals */
	  gboolean (*page_added) (EphyHistory * history, guint64 id);
	void (*page_removed) (EphyHistory * history, guint64 id);
	void (*page_updated) (EphyHistory * history, guint64 id);
};

GType ephy_history_get_type (void);

EphyHistory *ephy_history_new (void);

gboolean ephy_history_is_enabled (EphyHistory * eh);

gint ephy_history_get_page_visit_count (EphyHistory * eh, const char *url);

/* Not implemented: unused in ephy-history 
EphyHistoryPageNode *ephy_history_get_page          (EphyHistory *eh,
                                                     const char *url);

EphyHistoryPageNode *ephy_history_get_host          (EphyHistory *eh,
                                                     const char *url);
*/

GPtrArray *ephy_history_get_hosts (EphyHistory * eh);

GPtrArray *ephy_history_get_pages (EphyHistory * eh);

gboolean ephy_history_set_icon (EphyHistory * eh,
				const char *url, const char *icon);

const char *ephy_history_get_icon (EphyHistory * eh, const char *url);

void ephy_history_clear (EphyHistory * eh);

/*
 * nsIGlobalHistory2 
 */

gboolean ephy_history_add_page (EphyHistory * eh,
				const char *url,
				gboolean redirect,
				gboolean toplevel, const char *ref_url);

gboolean ephy_history_is_page_visited (EphyHistory * eh, const char *url);

gboolean ephy_history_set_page_title (EphyHistory * eh,
				      const char *url, const char *title);

/* 
 * New stuff
 */

GArray *ephy_history_get_page_ids (EphyHistory * eh);

EphyHistoryPageNode *ephy_history_get_page_by_id (EphyHistory * eh,
						  guint64 id);

gboolean ephy_history_remove_page (EphyHistory *eh, const char *url);

G_END_DECLS
#endif /* EPHY_HISTORY_H */
