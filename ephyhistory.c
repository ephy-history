/* -- THIS FILE IS GENERATED - DO NOT EDIT *//* -*- Mode: C; c-basic-offset: 4 -*- */

#include <Python.h>



#line 3 "ephyhistory.override"
#include <Python.h>               
#include "pygobject.h"
#include "ephy-history.h"
#include "ephy-history-node.h"
#include "ephy-history-page-node.h"
#include "ephy-history-visit-node.h"

static PyObject *
_helper_wrap_gobject_gptrarray (GPtrArray *list, gboolean dealloc)
{
    PyObject *py_list;
    int i;

    if ((py_list = PyList_New(0)) == NULL) {
        return NULL;
    }
    for( i = 0; i < list->len; i++ ) {
        PyObject *obj = pygobject_new (G_OBJECT (g_ptr_array_index(list, i)));
        PyList_Append(py_list, obj);
        Py_DECREF(obj);
    }
    if (dealloc) g_ptr_array_free (list, TRUE);
    return py_list;
}

static PyObject *
_helper_wrap_guint64_garray (GArray *list, gboolean dealloc)
{
    PyObject *py_list;
    int i;

    if ((py_list = PyList_New(0)) == NULL) {
        return NULL;
    }
    for( i = 0; i < list->len; i++ ) {
        PyList_Append (py_list, PyLong_FromUnsignedLongLong (g_array_index (list, guint64, i))); 
    }
    if (dealloc) g_array_free (list, TRUE);
    return py_list;
}
#line 49 "ephyhistory.c"


/* ---------- types from other modules ---------- */
static PyTypeObject *_PyGObject_Type;
#define PyGObject_Type (*_PyGObject_Type)


/* ---------- forward type declarations ---------- */
PyTypeObject G_GNUC_INTERNAL PyEphyHistory_Type;
PyTypeObject G_GNUC_INTERNAL PyEphyHistoryNode_Type;
PyTypeObject G_GNUC_INTERNAL PyEphyHistoryPageNode_Type;
PyTypeObject G_GNUC_INTERNAL PyEphyHistoryVisitNode_Type;

#line 63 "ephyhistory.c"



/* ----------- EphyHistory ----------- */

static int
_wrap_ephy_history_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char* kwlist[] = { NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,
                                     ":ephyhistory.History.__init__",
                                     kwlist))
        return -1;

    pygobject_constructv(self, 0, NULL);
    if (!self->obj) {
        PyErr_SetString(
            PyExc_RuntimeError, 
            "could not create ephyhistory.History object");
        return -1;
    }
    return 0;
}

static PyObject *
_wrap_ephy_history_is_enabled(PyGObject *self)
{
    int ret;

    
    ret = ephy_history_is_enabled(EPHY_HISTORY(self->obj));
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_ephy_history_get_page_visit_count(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", NULL };
    char *url;
    int ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"s:EphyHistory.get_page_visit_count", kwlist, &url))
        return NULL;
    
    ret = ephy_history_get_page_visit_count(EPHY_HISTORY(self->obj), url);
    
    return PyInt_FromLong(ret);
}

#line 52 "ephyhistory.override"
static PyObject *
_wrap_ephy_history_get_hosts (PyGObject *self)
{
    GPtrArray *list = ephy_history_get_hosts ((EphyHistory *) (self->obj));

    return _helper_wrap_gobject_gptrarray (list, FALSE);
}
#line 124 "ephyhistory.c"


#line 61 "ephyhistory.override"
static PyObject *
_wrap_ephy_history_get_pages (PyGObject *self)
{
    GPtrArray *list = ephy_history_get_pages ((EphyHistory *) (self->obj));

    return _helper_wrap_gobject_gptrarray (list, FALSE);
}
#line 135 "ephyhistory.c"


static PyObject *
_wrap_ephy_history_set_icon(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", "icon", NULL };
    char *url, *icon;
    int ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"ss:EphyHistory.set_icon", kwlist, &url, &icon))
        return NULL;
    
    ret = ephy_history_set_icon(EPHY_HISTORY(self->obj), url, icon);
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_ephy_history_get_icon(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", NULL };
    char *url;
    const gchar *ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"s:EphyHistory.get_icon", kwlist, &url))
        return NULL;
    
    ret = ephy_history_get_icon(EPHY_HISTORY(self->obj), url);
    
    if (ret)
        return PyString_FromString(ret);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_ephy_history_clear(PyGObject *self)
{
    
    ephy_history_clear(EPHY_HISTORY(self->obj));
    
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_ephy_history_add_page(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", "redirect", "toplevel", "ref_url", NULL };
    char *url, *ref_url;
    int redirect, toplevel, ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"siis:EphyHistory.add_page", kwlist, &url, &redirect, &toplevel, &ref_url))
        return NULL;
    
    ret = ephy_history_add_page(EPHY_HISTORY(self->obj), url, redirect, toplevel, ref_url);
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_ephy_history_is_page_visited(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", NULL };
    char *url;
    int ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"s:EphyHistory.is_page_visited", kwlist, &url))
        return NULL;
    
    ret = ephy_history_is_page_visited(EPHY_HISTORY(self->obj), url);
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_ephy_history_set_page_title(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "url", "title", NULL };
    char *url, *title;
    int ret;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"ss:EphyHistory.set_page_title", kwlist, &url, &title))
        return NULL;
    
    ret = ephy_history_set_page_title(EPHY_HISTORY(self->obj), url, title);
    
    return PyBool_FromLong(ret);

}

#line 70 "ephyhistory.override"
static PyObject *
_wrap_ephy_history_get_page_ids (PyGObject *self)
{
    GArray *list = ephy_history_get_page_ids ((EphyHistory *) (self->obj));

    return _helper_wrap_guint64_garray (list, FALSE);
}
#line 238 "ephyhistory.c"


static PyObject *
_wrap_ephy_history_get_page_by_id(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "id", NULL };
    PyObject *py_id = NULL;
    EphyHistoryPageNode *ret;
    guint64 id;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!:EphyHistory.get_page_by_id", kwlist, &PyLong_Type, &py_id))
        return NULL;
    id = PyLong_AsUnsignedLongLong(py_id);
    
    ret = ephy_history_get_page_by_id(EPHY_HISTORY(self->obj), id);
    
    /* pygobject_new handles NULL checking */
    return pygobject_new((GObject *)ret);
}

static const PyMethodDef _PyEphyHistory_methods[] = {
    { "is_enabled", (PyCFunction)_wrap_ephy_history_is_enabled, METH_NOARGS,
      NULL },
    { "get_page_visit_count", (PyCFunction)_wrap_ephy_history_get_page_visit_count, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "get_hosts", (PyCFunction)_wrap_ephy_history_get_hosts, METH_NOARGS,
      NULL },
    { "get_pages", (PyCFunction)_wrap_ephy_history_get_pages, METH_NOARGS,
      NULL },
    { "set_icon", (PyCFunction)_wrap_ephy_history_set_icon, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "get_icon", (PyCFunction)_wrap_ephy_history_get_icon, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "clear", (PyCFunction)_wrap_ephy_history_clear, METH_NOARGS,
      NULL },
    { "add_page", (PyCFunction)_wrap_ephy_history_add_page, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "is_page_visited", (PyCFunction)_wrap_ephy_history_is_page_visited, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "set_page_title", (PyCFunction)_wrap_ephy_history_set_page_title, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "get_page_ids", (PyCFunction)_wrap_ephy_history_get_page_ids, METH_NOARGS,
      NULL },
    { "get_page_by_id", (PyCFunction)_wrap_ephy_history_get_page_by_id, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { NULL, NULL, 0, NULL }
};

PyTypeObject G_GNUC_INTERNAL PyEphyHistory_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "ephyhistory.History",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)_PyEphyHistory_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_ephy_history_new,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- EphyHistoryNode ----------- */

PyTypeObject G_GNUC_INTERNAL PyEphyHistoryNode_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "ephyhistory.HistoryNode",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)NULL, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)0,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- EphyHistoryPageNode ----------- */

static int
_wrap_ephy_history_page_node_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "id", "uri", "title", "favicon_uri", "visit_count", "last_visit", NULL };
    PyObject *py_id = NULL, *py_visit_count = NULL, *py_last_visit = NULL;
    char *uri, *title, *favicon_uri;
    guint visit_count = 0;
    guint64 id, last_visit;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!sssOO!:EphyHistoryPageNode.__init__", kwlist, &PyLong_Type, &py_id, &uri, &title, &favicon_uri, &py_visit_count, &PyLong_Type, &py_last_visit))
        return -1;
    id = PyLong_AsUnsignedLongLong(py_id);
    if (py_visit_count) {
        if (PyLong_Check(py_visit_count))
            visit_count = PyLong_AsUnsignedLong(py_visit_count);
        else if (PyInt_Check(py_visit_count))
            visit_count = PyInt_AsLong(py_visit_count);
        else
            PyErr_SetString(PyExc_TypeError, "Parameter 'visit_count' must be an int or a long");
        if (PyErr_Occurred())
            return -1;
    }
    last_visit = PyLong_AsUnsignedLongLong(py_last_visit);
    self->obj = (GObject *)ephy_history_page_node_new(id, uri, title, favicon_uri, visit_count, last_visit);

    if (!self->obj) {
        PyErr_SetString(PyExc_RuntimeError, "could not create EphyHistoryPageNode object");
        return -1;
    }
    pygobject_register_wrapper((PyObject *)self);
    return 0;
}

static PyObject *
_wrap_ephy_history_page_node_free(PyGObject *self)
{
    
    ephy_history_page_node_free(EPHY_HISTORY_PAGE_NODE(self->obj));
    
    Py_INCREF(Py_None);
    return Py_None;
}

static const PyMethodDef _PyEphyHistoryPageNode_methods[] = {
    { "free", (PyCFunction)_wrap_ephy_history_page_node_free, METH_NOARGS,
      NULL },
    { NULL, NULL, 0, NULL }
};

PyTypeObject G_GNUC_INTERNAL PyEphyHistoryPageNode_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "ephyhistory.HistoryPageNode",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)_PyEphyHistoryPageNode_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_ephy_history_page_node_new,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- EphyHistoryVisitNode ----------- */

PyTypeObject G_GNUC_INTERNAL PyEphyHistoryVisitNode_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "ephyhistory.HistoryVisitNode",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)NULL, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)0,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- functions ----------- */

const PyMethodDef ephyhistory_functions[] = {
    { NULL, NULL, 0, NULL }
};

/* initialise stuff extension classes */
void
ephyhistory_register_classes(PyObject *d)
{
    PyObject *module;

    if ((module = PyImport_ImportModule("gobject")) != NULL) {
        _PyGObject_Type = (PyTypeObject *)PyObject_GetAttrString(module, "GObject");
        if (_PyGObject_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name GObject from gobject");
            return ;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gobject");
        return ;
    }


#line 556 "ephyhistory.c"
    pygobject_register_class(d, "EphyHistory", EPHY_TYPE_HISTORY, &PyEphyHistory_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pyg_set_object_has_new_constructor(EPHY_TYPE_HISTORY);
    pygobject_register_class(d, "EphyHistoryNode", EPHY_TYPE_HISTORY_NODE, &PyEphyHistoryNode_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pyg_set_object_has_new_constructor(EPHY_TYPE_HISTORY_NODE);
    pygobject_register_class(d, "EphyHistoryPageNode", EPHY_TYPE_HISTORY_PAGE_NODE, &PyEphyHistoryPageNode_Type, Py_BuildValue("(O)", &PyEphyHistoryNode_Type));
    pygobject_register_class(d, "EphyHistoryVisitNode", EPHY_TYPE_HISTORY_VISIT_NODE, &PyEphyHistoryVisitNode_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pyg_set_object_has_new_constructor(EPHY_TYPE_HISTORY_VISIT_NODE);
}
