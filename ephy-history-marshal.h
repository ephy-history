#ifndef __EPHY_HISTORY_MARSHAL_H__
#define __EPHY_HISTORY_MARSHAL_H__

#include        <glib-object.h>

G_BEGIN_DECLS

/* VOID:UINT64 (/dev/stdin:1) */
extern void g_cclosure_marshal_VOID__UINT64 (GClosure     *closure,
                                                  GValue       *return_value,
                                                  guint         n_param_values,
                                                  const GValue *param_values,
                                                  gpointer      invocation_hint,
                                                  gpointer      marshal_data);

G_END_DECLS

#endif
