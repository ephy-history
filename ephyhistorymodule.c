#include <pygobject.h>
 
void ephyhistory_register_classes (PyObject *d); 
extern PyMethodDef ephyhistory_functions[];
 
DL_EXPORT(void)
initephyhistory(void)
{
    PyObject *m, *d;
 
    init_pygobject ();
 
    m = Py_InitModule ("ephyhistory", ephyhistory_functions);
    d = PyModule_GetDict (m);
 
    ephyhistory_register_classes (d);
 
    if (PyErr_Occurred ()) {
        Py_FatalError ("can't initialise module ephyhistory");
    }
}

